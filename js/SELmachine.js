document.write("<script language=javascript src='./js/config.js'></script>");
angular.module("selmachine", ['ng', 'ngRoute', 'ngAnimate']).config(function ($routeProvider) {
    $routeProvider.when('/main', {
        templateUrl: 'tpl/main.html',
        controller: 'mainCtr'
    }).when('/inputCarNum', {
        templateUrl: 'tpl/inputCarNum.html',
        controller: 'inputCarNumCtr'
    }).when('/confirmCarNum', {
        templateUrl: 'tpl/confirmCarNum.html',
        controller: 'confirmCarNumCtr'
    }).when('/confirmPayInfo', {
        templateUrl: 'tpl/confirmPayInfo.html',
        controller: 'confirmPayInfoCtr'
    }).when('/scanBarCode', {
        templateUrl: 'tpl/scanBarCode.html',
        controller: 'scanBarCodeCtr'
    }).when('/line', {
        templateUrl: 'tpl/line.html',
        controller: 'lineCtr'
    }).otherwise({redirectTo: '/main'})
}).controller('mainCtr', function ($scope, $location, $rootScope, $timeout) {
    $scope.jumpFirst = function (url, gostyle) {
        if (gostyle == 'pay') {
            $scope.clickpayBTN = true;
            $timeout(function () {
                $scope.clickpayBTN = false;
            }, 1000);
        }
        if (gostyle == "find") {
            $location.path(url);
            $rootScope.sty = gostyle;
        }


    }

}).controller('inputCarNumCtr', function ($scope, $rootScope, $location) {
    $scope.inputCarNum = "";
    $scope.str = "";
    $scope.placeH = "请从键盘输入车牌号";
    $scope.inputBtn = function (CAN) {
        $scope.isThree = false;
        $scope.placeH = '';
        if ($scope.inputCarNum.length < 6) {
            $scope.inputCarNum = $scope.inputCarNum + CAN;
            $scope.str = $scope.inputCarNum;
        } else {
            $scope.isSix = true;
        }
    }
    //删除
    $scope.delectAll = function () {
        $scope.isThree = false;
        $scope.isSix = false;
        $scope.inputCarNum = "";
        $scope.placeH = "请从键盘输入车牌号";
    }

    $scope.toleft = function () {
        $scope.isThree = false;
        $scope.isSix = false;
        $scope.inputCarNum = $scope.inputCarNum.substr(0, $scope.inputCarNum.length - 1);
        if ($scope.inputCarNum.length == 0) {
            $scope.placeH = "请从键盘输入车牌号";
        }
    }
    $scope.toRigth = function () {
        $scope.isThree = false;
        $scope.isSix = false;
        $scope.placeH = "";
        $scope.inputCarNum = $scope.str.substr(0, $scope.inputCarNum.length + 1);
    }
    //确认
    $scope.confirmCarN = function () {
        $scope.isSix = false;
        if ($scope.inputCarNum.length < 3) {
            $scope.isThree = true;
        } else {
            $location.path('/confirmCarNum');
            $rootScope.searchCarNum = $scope.inputCarNum;
        }
    }
    $scope.jumpback = function (url) {

        window.history.go(-1);
    }
}).controller('confirmCarNumCtr', function ($scope, $http, $rootScope, $location) {
    var n = 0;
    var m = 0;
    var index = 0;
//请求数据
    $http.get("http://" + server_ip + "/SELmachine/1.php?carNum=" + $rootScope.searchCarNum).success(function (data) {
        if (data.length != 0) {
            $scope.tishi = '(请点击图片选择您的车辆)';
            $scope.datas = data;
            //如果数据少于3条，
            if ($scope.datas.length <= 3) {
                n = 3;
                $scope.dataF = $scope.datas;

            } else {
                $scope.dataF = [$scope.datas[0], $scope.datas[1], $scope.datas[2]];

                n = 3;
            }
        } else {

            $scope.tishi = '未查询到您的车辆信息，请重新输入';
        }
    });
    // 上一页
    $scope.backimages = function () {

        if (n > 3) {
            n = n - m;
            $scope.dataF = [$scope.datas[n - 3], $scope.datas[n - 2], $scope.datas[n - 1]];
            m = 3;

        }
    }
    //下一页
    $scope.nextimages = function () {
        if ($scope.datas.length >= n + 3) {
            $scope.dataF = [$scope.datas[n], $scope.datas[n + 1], $scope.datas[n + 2]];
            n = n + 3;
            m = 3;
        }
        if ($scope.datas.length == n + 1) {
            $scope.dataF.dataF = [$scope.datas[n]];
            m = 1;
            n = n + 1;
        }
        if ($scope.datas.length == n + 2) {
            $scope.dataF = [$scope.datas[n], $scope.datas[n + 1]];
            n = n + 2;
            m = 2;
        }
    }
    $scope.showImage = function (index) {
        index = index;
        $rootScope.location = $scope.dataF[index].Cam_ID;
        $scope.bigimageUrl = $rootScope.imageurl = $scope.dataF[index].Pic_Path;
        $scope.showbigImage = true;
    }
    $scope.closeBigImage = function () {
        $scope.showbigImage = false;
    }
    $scope.jumpto = function (gostyle) {
        $rootScope.carNum = $scope.dataF[index].Plate_No1;
        $rootScope.imageurl = $scope.dataF[index].Pic_Path;
        $rootScope.happentime = $scope.dataF[index].Happen_Time;


        if (gostyle == "go") {
            if ($rootScope.sty == 'pay') {
                $location.path("/confirmPayInfo");
            } else if ($rootScope.sty == 'find') {


                $location.path('/line');
            }

        } else if (gostyle == 'backmain') {
            window.history.go(-2);
        }
    }
//重新输入
    $scope.rescan = function (url) {
        $rootScope.searchCarNum = "";
        $scope.datas = null;
        $scope.dataF = null;
        window.history.go(-1);

    }


}).controller('scanBarCodeCtr', function ($scope, $timeout, $location, $http) {


}).controller('confirmPayInfoCtr', function ($scope, $rootScope, $location, $http, $timeout) {


    $scope.localDate = new Date();
    $scope.jump = function (url) {
        if (url == '/confirmCarNum') {
            window.history.go(-1);
        } else if (url == '/line') {
            window.history.go(-3);
        }
    }


    Date.prototype.format = function (format) {
        var args = {
            "M+": this.getMonth() + 1,
            "d+": this.getDate(),
            "h+": this.getHours(),
            "m+": this.getMinutes(),
            "s+": this.getSeconds(),
        };
        if (/(y+)/.test(format))
            format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var i in args) {
            var n = args[i];
            if (new RegExp("(" + i + ")").test(format))
                format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? n : ("00" + n).substr(("" + n).length));
        }
        return format;
    };
    $scope.out_trade_no = 'longtone' + new Date().format("yyyyMMddhhmmss") + $rootScope.carNum + '20';


}).controller('lineCtr', function ($scope, $rootScope, $location) {
    $scope.url = "http://" + server_ip + "/SELmachine/path_planning.php?camera_id=" + $rootScope.location + "&start=m-1";
    $scope.jump = function (index) {
        if (index == '1') {
            window.history.go(-1);
        }
        if (index == '2') {
            window.history.go(-3);
        }

    }

    $scope.fangda = function () {
        if (heig < 500) {
            heig += 20;
            wid += 20;
            paddl = (1100 - wid) / 2;
            paddt = (500 - heig) / 2;
            $('#canvas1').width(wid);
            $('#canvas1').height(heig);
            $('#canvascontainer').css('padding-top', paddt);
            $('#canvascontainer').css('padding-left', paddl);
        }
    }
    $scope.suoxiao = function () {
        if (heig >= 250) {
            heig -= 20;
            wid -= 20;
            paddl = (1100 - wid) / 2;
            paddt = (500 - heig) / 2;
            $('#canvas1').width(wid);
            $('#canvas1').height(heig);
            $('#canvascontainer').css('padding-top', paddt);
            $('#canvascontainer').css('padding-left', paddl);
        }
    }

})