<?php
// 指定允许其他域名访问
require 'db_config.php';
header('Access-Control-Allow-Origin:*');
$carnum = $_GET['carNum'];/*
$link = mysqli_connect($server, $db_user, $db_pwd, $dbname, "3306");

$sql = "SELECT Pic_Path, Plate_No1,Happen_Time,Cam_ID FROM Plate_Log where Plate_No1 like '%$carnum%'";
$result = mysqli_query($link, $sql);
$arr = [];

while ($row = mysqli_fetch_assoc($result)) {
    $str1 = substr($row['Pic_Path'], 40);
    for ($i = 0; $i < strlen($str1); $i++) {
        if ($str1[$i] == "\\")
            $str1[$i] = "/";
    }
    $row["Pic_Path"] = "http://192.168.1.110/picture/" . $str1;
    $arr[] = $row;
}
echo json_encode($arr);
*/
$pdo=new PDO($db,$db_user,$db_pwd);
$pdo->exec("set character set gbk");
$threeDaysBefore=date("Y-m-d H:i:s",strtotime("-3days"));
$fiveDaysLater=date("Y-m-d H:i:s",strtotime("+5days"));
//未筛选
$sql = "SELECT Pic_Path, Plate_No1,Happen_Time,Cam_ID FROM Plate_Log where Plate_No1 like '%$carnum%'  order by Happen_Time DESC";
/*$sql = "SELECT Pic_Path, Plate_No1,Happen_Time,Cam_ID FROM Plate_Log where Plate_No1 like '%$carnum%' AND (Happen_Time > '$threeDaysBefore' AND Happen_Time < '$fiveDaysLater')  order by Happen_Time DESC";*/
$res=$pdo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
foreach($res as $k=>$v){
    $res[$k]['Plate_No1']=iconv('gbk', 'utf-8', $res[$k]['Plate_No1']);
    $pic_path=str_replace("\\","/",$v['Pic_Path']);
    $pic_path=str_replace("/Share","",$pic_path);
    $res[$k]['Pic_Path']='http:'.$pic_path;
}
echo json_encode($res,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);