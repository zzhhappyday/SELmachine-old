<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017-07-05
 * Time: 10:15
 */
require 'db_config.php';
require 'lib/dijkstra.class.php';
require 'lib/draw.class.php';
ob_clean();
header('Content-Type: image/png;');
$pdo=new PDO($db,$db_user,$db_pwd);

$dij=new dijkstra($pdo);
$draw=new draw();
$start=$_GET['start'];
$camera_id=$_GET['camera_id'];
//获取结束节点
$end=$dij->endNode($camera_id);
    ob_end_clean();
    //取得文件名
    $pic_name=$dij->getPicName($end);
    $pic=imagecreatefrompng("./img/".$pic_name);
    $black = imagecolorallocate($pic, 0, 0, 0);
    $red = imagecolorallocate($pic, 255, 0, 0);
    $white = imagecolorallocate($pic, 255, 255, 255);
    $font = './font/simfang.ttf';
    if($dij->getFlatId($start)==$dij->getFlatId($end)){
        $path=$dij->dijkstra($start,$end);
        $count=count($path['node']);
        $start_x='';
        $start_y='';
        $end_x='';
        $end_y='';
        $start_node = '当前位置';
        $end_node = '车辆所在位置';
        $startflag='';
        for($i=0;$i<$count-1;$i++){
            $line=$dij->getLine($path['node'][$i],$path['node'][$i+1]);
            //遍历线段
            foreach($line as $v){
                $node_path=explode(',',$v['line']);
                $x1=explode('-',$node_path['0']);
                $x1=$x1['0'];
                $y1=explode('-',$node_path['0']);
                $y1=$y1['1'];
                $x2=explode('-',$node_path['1']);
                $x2=$x2['0'];
                $y2=explode('-',$node_path['1']);
                $y2=$y2['1'];
                $draw->imagelinethick($pic,$x1,$y1,$x2,$y2,$red,10);
                if($i==0&&$startflag!='1'){
                    $start_x=$x1;
                    $start_y=$y1;
                    $startflag='1';
                }
                if($i==$count-2){
                    $end_x=$x2;
                    $end_y=$y2;
                }
            }

        }
        $draw->addIcon($pic,$start_x,$start_y,'start');
        $draw->addIcon($pic,$end_x,$end_y,'end');
        //翻转
        $pic=imagerotate($pic,180,$white);
    }else{
        $position=$dij->getPosition($end);
        $end_x=$position['x'];
        $end_y=$position['y'];
        $draw->addIcon($pic,$end_x,$end_y,'end');
        $end_id=$dij->getFlatId($end);
        $flatName=$dij->getFlatName($end_id);
        $flatName=iconv('gbk', 'utf-8', $flatName);
        $pic=imagerotate($pic,180,$white);
        imagettftext($pic, 55, 0, 200, 250, $red, $font, '您查询的车辆不在此楼层');
        imagettftext($pic, 55, 0, 200, 350, $red, $font, '请至('.$flatName.')进行查询');
    }
/*    imagettftext($pic, 14, 0, $start_x, $start_y, $red, $font, $start_node);
    imagettftext($pic, 14, 0, $end_x, $end_y, $red, $font, $end_node);*/
    imagepng($pic);
    imagedestroy($pic);


