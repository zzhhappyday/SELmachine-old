<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017-07-19
 * Time: 13:14
 */
class draw
{
    public function addIcon($image,$x,$y,$name){
        $im=imagecreatefrompng('./img/'.$name.'.png');
        $white = imagecolorallocate($im , 254 , 254 , 254);
        imagefill($im , 0 , 0 , $white);//把画布染成白色
        imagecolortransparent($im,$white);
        return imagecopymerge($image,$im,$x-30,$y-11,0,0,57,57,100);
    }
    //画粗线
    public function imagelinethick($image, $x1, $y1, $x2, $y2, $color, $thick = 1)
    {
        /* 下面两行只在线段直角相交时好使
        imagesetthickness($image, $thick);
        return imageline($image, $x1, $y1, $x2, $y2, $color);
        */
        if ($thick == 1) {
            return imageline($image, $x1, $y1, $x2, $y2, $color);
        }
        $t = $thick / 2 - 0.5;
        if ($x1 == $x2 || $y1 == $y2) {
            return imagefilledrectangle($image, round(min($x1, $x2) - $t), round(min($y1, $y2) - $t), round(max($x1, $x2) + $t), round(max($y1, $y2) + $t), $color);
        }
        $k = ($y2 - $y1) / ($x2 - $x1); //y = kx + q
        $a = $t / sqrt(1 + pow($k, 2));
        $points = array(
            round($x1 - (1+$k)*$a), round($y1 + (1-$k)*$a),
            round($x1 - (1-$k)*$a), round($y1 - (1+$k)*$a),
            round($x2 + (1+$k)*$a), round($y2 - (1-$k)*$a),
            round($x2 + (1-$k)*$a), round($y2 + (1+$k)*$a),
        );
        imagefilledpolygon($image, $points, 4, $color);
        return imagepolygon($image, $points, 4, $color);
    }
}