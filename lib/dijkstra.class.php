<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017-06-20
 * Time: 9:58
 */
class dijkstra
{
    //顶点
    public $points=array();
    //到各点的距离
    public $dist=array();
    //已完成的点
    public $done=array();
    public function __construct($db)
    {
        $this->db=$db;
        $this->db->exec("set names gbk");
    }
//取得路径
    public function getLine($start,$end){
        $sql="select next_nodes from node_info where node_id='$start'";
        $res=$this->db->query($sql)->fetch(PDO::FETCH_COLUMN);
        $res=json_decode($res,true);
        foreach($res as $v){
            if($end==$v['node_id']){
                return $v['path'];
            }
        }
    }
    //获取结束节点
    public function endNode($camera_id){
        $sql="select node_id from camera_node where camera_id = '$camera_id'";
        $res=$this->db->query($sql)->fetch(PDO::FETCH_COLUMN);
        return $res;
    }
    //取得地图编号
    public function getFlatId($node){
        $sql="select FlatId from node_info where node_id = '$node'";
        $res=$this->db->query($sql)->fetch(PDO::FETCH_COLUMN);
        return $res;
    }
//取得图片名称
    public function getPicName($start){
        $FlatId=$this->getFlatId($start);
        $sql="select FileName from Flat where Flat_Id = '$FlatId'";
        $res=$this->db->query($sql)->fetch(PDO::FETCH_COLUMN);
        return $res;
    }
//获取节点
    public function getNodes($FlatId){
        $sql="select * from node_info where FlatId = $FlatId";
        $res=$this->db->query($sql)->fetchall(PDO::FETCH_ASSOC);
        $nodes=array();
        foreach($res as $v){
            $nodes[$v['node_id']]=array();
            $next_nodes=array();
            $next_nodes=json_decode($v['next_nodes'],true);
            foreach($next_nodes as $val){
                $nodes[$v['node_id']][$val['node_id']]=$val['dist'];
            }
        }
        return $nodes;
    }
//路径规划
    public function  dijkstra($start,$end){
        $FlatId=$this->getFlatId($start);
        //顶点
        $this->points=$this->getNodes($FlatId);
        //当前顶点
        $current='';
        //最小值
        $min='';
        $min_key='';
        foreach($this->points as $node=>$next){
            //初始化
            $this->dist[$node]=array(
                'dist'=>'',
                'node'=>array()
            );
            //寻找开始点
            if($node==$start){
                $start_node=$node;
                $next_nodes=$next;
            }
        }
        //将初始点加入路径

        array_push($this->done,$start_node);
        $current=$this->done[0];
        foreach($this->dist as $k=>$v){
            if($k==$start_node){
                $this->dist[$k]['dist']='0';
            }
            array_push($this->dist[$k]['node'],$start_node);
        }
        //写入路径数组
        foreach($next_nodes as $k=>$v){
            $this->dist[$k]['dist']=$v;
            array_push($this->dist[$k]['node'],$k);
        }
        while(count($this->done)<count($this->points)){
            //遍历当前点数组，找最小值$k(邻接点)$v(距离)
            foreach($this->points[$current] as $k=>$v){
                //如果目前的最近距离大于经过当前点的最近距离，或目前最近距离为空则更新距离
/*                if($this->dist[$k]['dist']>$this->dist[$current]['dist']+$v||$this->dist[$k]['dist']==''){
                    //重新设置距离
                    $this->dist[$k]['dist']=$this->dist[$current]['dist']+$v;
                    //重新设置路径
                    $this->dist[$k]['node']=$this->dist[$current]['node'];
                    array_push( $this->dist[$k]['node'],$k);
                }*/
                //到下一个点的条件
                if(($min==''||$v-$min<0)&&!in_array($k,$this->done)){
                    $min_key=$k;
                    $min=$v;
                }
            }
            //如果没有找到下一个点，回退一个点
            if($min_key==''){
                for($i=0;$i<count($this->done);$i++){
                    if($this->done[$i]==$current){
                        $current=$this->done[$i-1];
                    }
                }
            }else{
                array_push($this->done,$min_key);
                $current=$min_key;
            }
            $this->pathRebuild($current);
            $min_key='';
            $min='';
        }
        return $this->dist[$end];
    }
//获取点坐标
    public function getPosition($node){
        $sql="select x,y from node_info where node_id = '$node'";
        $res=$this->db->query($sql)->fetch(PDO::FETCH_ASSOC);
        return $res;
    }
    //获取地图名
    public function getFlatName($FlatId){
        $sql="select Flat_Name from Flat where Flat_Id = '$FlatId'";
        $res=$this->db->query($sql)->fetch(PDO::FETCH_COLUMN);
        return $res;
    }

    private function pathRebuild($currentPoint){
        foreach($this->points[$currentPoint] as $nextNode=>$distance){
            //如果目前的最近距离大于经过当前点的最近距离，或目前最近距离为空则更新距离
            if($this->dist[$nextNode]['dist']>$this->dist[$currentPoint]['dist']+$distance||$this->dist[$nextNode]['dist']==''){
                //重新设置距离
                $this->dist[$nextNode]['dist']=$this->dist[$currentPoint]['dist']+$distance;
                //重新设置路径
                $this->dist[$nextNode]['node']=$this->dist[$currentPoint]['node'];
                array_push($this->dist[$nextNode]['node'],$nextNode);
                //递归
                $this->pathRebuild($nextNode);
            }
        }
    }
}
